include $(call all-subdir-makefiles)

VENDOR_PATH := vendor/android-generic

# Overlays
PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += $(VENDOR_PATH)/overlay
DEVICE_PACKAGE_OVERLAYS += $(VENDOR_PATH)/overlay/common

# First, make sure we grab anything that may be in vendor/bliss
$(foreach f,$(wildcard vendor/bliss/config/permissions/*.xml),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/$(notdir $f)))
# Then we copy all Bliss-specific Permissions files, overriding anything from Bliss if needed
$(foreach f,$(wildcard $(VENDOR_PATH)/system/etc/permissions/*.xml),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/$(notdir $f)))

# Copy all Bliss-specific init rc files
$(foreach f,$(wildcard $(VENDOR_PATH)/system/etc/*.xml),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/etc/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/etc/alsa/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/etc/alsa/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/etc/init/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/etc/init/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/etc/init.d/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/etc/init.d/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/bin/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/bin/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/lib/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/lib/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/media/audio/alarms/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/media/alarms/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/media/audio/notifications/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/media/notifications/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/media/audio/ringtones/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/media/ringtones/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/usr/idc/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/usr/idc/$(notdir $f)))

$(foreach f,$(wildcard $(VENDOR_PATH)/system/usr/keylayout/*),\
    $(eval PRODUCT_COPY_FILES += $(f):$(TARGET_COPY_OUT_SYSTEM)/usr/keylayout/$(notdir $f)))

