<img src="https://i.imgur.com/bTd1SMo.png">

# Android-Generic (x86) 1.0 - Android for PCs (& GSI devices - coming soon)

-----------

# Table of Contents

------
[Getting Started](#Getting Started)
-----

[Prerequisites](#Prerequisites:)

[What you need to build with](#What you need to build with)

[Getting The Repository](#Getting The Repository)

------
[Instructions For Setting Up Your Environment For PC Builds](#Instructions For Setting Up Your Environment For PC Builds)
------

[Proprietary PC Files](#Proprietary Files)

[Building Android For PC Explained](#Building Android For PC)

[Getting Your Build Going](#Now For The Fun Stuff, Getting Your Build Going)

------
[Adding your vendor to Android-Generic](#Adding your vendor to Android-Generic)
------

[Introduction to adding your vendor support](#Introduction to adding your vendor support)

[Updating the PC Scripts](#Updating the PC Scripts)

[Updating the PC Manifests](#Updating the PC Manifests)

[Resolving Patch Conflicts](#Resolving Patch Conflicts)

-------
[Updating the Script Repo](#Updating the Script Repo)
-------

------
[Credits](#Credits)
------

# Getting Started

This portion of the instructions is for both PC & GSI builds. 

### Prerequisites:

You will need to have synced a ROM prior to adding this to your build envirnment. 

For PC builds (so far):
	
	- BlissROM
	- AOSP
	- Tesla
	- Tipsy
	- Lineage OS

For GSI builds (so far):
	
	- BlissROM
	- AOSP

Please make sure you're well versed in building AOSP: [AOSP building instructions](http://source.android.com/source/index.html) before proceeding.

### What you need to build with 

[android-generic](https://gitlab.com/android-generic/vendor_android-generic)

    Latest Ubuntu LTS Releases https://www.ubuntu.com/download/server
    Decent CPU (Dual Core or better for a faster performance)
    8GB RAM (16GB for Virtual Machine)
    250GB Hard Drive (about 170GB for the Repo and then building space needed)
  
#### Installing Java 8

    sudo apt-get update && upgrade
    sudo apt-get install openjdk-8-jdk-headless
    update-alternatives --config java  (make sure Java 8 is selected)
    update-alternatives --config javac (make sure Java 8 is selected)
    
#### Grabbing Dependencies

    $ sudo apt-get install git-core gnupg flex bison maven gperf build-essential zip curl zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386  lib32ncurses5-dev x11proto-core-dev libx11-dev lib32z-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip squashfs-tools python-mako libssl-dev ninja-build lunzip syslinux syslinux-utils gettext genisoimage gettext bc xorriso libncurses5 xmlstarlet build-essential git imagemagick lib32readline-dev lib32z1-dev liblz4-tool libncurses5-dev libsdl1.2-dev libxml2 lzop pngcrush rsync schedtool python-enum34 python3-mako libelf-dev
    
#### Setting Up Cache

Add these lines to your `.bashrc` file:

    ccache -F 0 && ccache -M 0 
	export CCACHE_DIR=$HOME/.ccache
	export USE_CCACHE=1 
	export CCACHE_TEMPDIR=/tmp 
	export EXPERIMENTAL_USE_JAVA8=true
	
#### Installing and Initializing Repo

Follow the instructions on the Android Source Build site to install `repo` and setting up your distribution's manifest.

    https://source.android.com/setup/build/downloading#installing-repo

### Getting The Repository

Starting from your project folder, follow each step as follows:

Clone Repo :
    
	$ git clone https://gitlab.com/android-generic/vendor_android-generic vendor/android-generic

Setup Build Envirnment :

    $ . build/envsetup.sh
    
# Instructions For Setting Up Your Environment For PC Builds

To start, you must first use the -s (--sync) flag, then on following builds, it is not needed. 

You can do this two ways. Traditionally or using the build script. Point is, you must sync with the new manifest changes. 

First, sync your ROM's manifest first :

	$ repo sync --no-tags --no-clone-bundle --force-sync

Then sync using the build script to add in all the Android-Generic additions :

	$ build-x86 -s

At this point it will ask you what your device target is (Atom, Generic, Vulkan, etc) 

    Just answer with 1,2,3,etc. 

### Proprietary Files

(Houdini & Widevine from Chrome OS)

Initial extraction of the proprietary files from Google are also needed on the first build. 
Please note that this process will need to be run for x86 & x86_64 builds seperately (some cleanup may be needed). 

We are able to use the -r (--proprietary) flag for that. This step needs to be done once per device setup (x86 or x86_64) and we have it working on its own because
the image mounting process requires root permissions, so keep a look out for it asking for your root password. 

Grabbing Proprietary Files (make sure you add your target here; android_x86 or android_x86_64) :

    $ build-x86 -r android_x86_64-userdebug

### Building Android For PC

PC builds (x86) explained:
	  
This script will allow you to build an x86 based .ISO for PCs as well as help you with a few other things. Please see details below

#### Usage :

	$ build-x86 options buildVariants extraOptions addons

#### Options : 

	-c | --clean : Does make clean && make clobber and resets the device tree
	-s | --sync: Repo syncs the rom (clears out patches), then reapplies patches to needed repos
	-p | --patch: Just applies patches to needed repos
	-r | --proprietary: build needed items from proprietary vendor (non-public)
    -o | --oldproprietary: build needed items from old proprietary vendor/bliss_priv for ChromeOS houdini & widevine
    -d | --desktopmode: build with desktop mode enabled by default
    -i | --iptsdrivers: build with Intel IPTS kernel modules added
    -e | --efi_img: build as an EFI .img file (depends on branch of bootable/newinstaller)
    -u | --subsync: forces all submodules for ax86-nb-qemu to init & sync
	-k | --kernel: build with specific kernel branch

#### BuildVariants :

	android_x86-user : Make user build
	android_x86-userdebug |: Make userdebug build
	android_x86-eng : Make eng build
	android_x86_64-user : Make user build
	android_x86_64-userdebug |: Make userdebug build
	android_x86_64-eng : Make eng build

#### ExtraOptions : Defaults to None

	foss : packages microG & FDroid with the build
	fdroid : packages custom FDroid with the build (requires private sources)
	go : packages Gapps Go with the build (when go vendor is synced)
	gapps : packages OpenGapps with the build (when OpenGapps vendor sources are synced)
	gms : packages GMS with the build (requires private repo access)
	none : force all extraOption flags to false. (Drfault Option)

#### Addons : Requires "--proprietary" build to have run at least once. Defaults to None

	croshoudini : Include libhoudini from Chrome OS 
	croswidevine : Include widevine from Chrome OS
	crosboth : Include both libhoudini and widevine from Chrome OS
    crospriv : Include both libhoudini and widevine from old vendor/bliss_priv method for Chrome OS 
	crosnone : Do not include any of them. (Default Option)
    x86nb : Use ax86-NB Source: https://github.com/goffioul/ax86-nb-qemu

## Now For The Fun Stuff, Getting Your Build Going

Once you have gone through all the above steps (syncing, patching, proprietary files). You are ready to start your first build of Android for PC. Use the options above to specify what kind of build you want. 

(to build the userdebug version for x86_64 CPUs with FDroid & microG, along with widevine from ChromeOS included)

	$ build-x86 android_x86_64-userdebug foss croswidevine 

(to build the userdebug version for x86_64 CPUs with Play Store and widevine from ChromeOS included)

	$ build-x86 android_x86_64-userdebug gms croswidevine 

(to build the userdebug version for x86 CPUs with Play Store and ax86-nativebridge included, and choose a different kernel branch upon build-time)

	$ build-x86 android_x86-userdebug -k gms x86nb 


---------
# Adding your vendor to Android-Generic
---------

## Introduction to adding your vendor support

For ROMs not already added to AG, there are a few things that you will need to update in the source before things are ready to patch on your side.

PC Patches:

	* Vendor specific patches ( vendor/android-generic/patches/google_diff/pc_vendor_prepatches/__YourVendorName__ )
	* Vendor specific manifests ( vendor/android-generic/manifests/android_pc/__YourVendorName__ )
    * Vendor specific patches ( vendor/android-generic/patches/google_diff/pc_vendor_patches/__YourVendorName__ )

GSI Patches:

	* Vendor specific patches ( vendor/android-generic/patches/google_diff/treble_vendor_prepatches/__YourVendorName__ )
	* Vendor specific manifests ( vendor/android-generic/manifests/treble/__YourVendorName__ )
    * Vendor specific patches ( vendor/android-generic/patches/google_diff/treble_vendor_patches/__YourVendorName__ )

To break things down a little, let's take things one at a time, but starting with updating the script, then the Manifests and Patches. 

### Updating the PC Scripts

Next step is to add your vendor to the build script. 

    $ nano vendor/android-generic/build-x86.sh

Then scroll down to the the first "if  [ $sync == "y" ];then" section and add your vendor to the following list: 

        if [ -d $rompath/.repo/local_manifests ] ;then
	    echo -e ${CL_CYN}""${CL_RST}

	    if [ -d $rompath/vendor/bliss/ ]; then
	      # Take action if $DIR exists. #
	      export ROM_IS_BLISS=true
	      echo -e ${CL_CYN}"copying bliss specific manifest files..."${CL_RST}
	      cp -r $rompath/vendor/$vendor_path/manifests/android_pc/bliss/* $rompath/.repo/local_manifests
	    fi
	    if [ -d $rompath/vendor/lineage/ ]; then
	      # Take action if $DIR exists. #
	      export ROM_IS_LINEAGE=true
	      echo -e ${CL_CYN}"copying lineage specific manifest files..."${CL_RST}
	      cp -r $rompath/vendor/$vendor_path/manifests/android_pc/lineage/* $rompath/.repo/local_manifests
	    fi
	    if [ -d $rompath/vendor/tesla/ ]; then
	      # Take action if $DIR exists. #
	      export ROM_IS_TESLA=true
	      echo -e ${CL_CYN}"copying tesla specific manifest files..."${CL_RST}
	      cp -r $rompath/vendor/$vendor_path/manifests/android_pc/tesla/* $rompath/.repo/local_manifests
	    fi
	    if [ -d $rompath/vendor/tipsy/ ]; then
	      # Take action if $DIR exists. #
	      export ROM_IS_TIPSY=true
	      echo -e ${CL_CYN}"copying tipsy specific manifest files..."${CL_RST}
	      cp -r $rompath/vendor/$vendor_path/manifests/android_pc/tipsy/* $rompath/.repo/local_manifests
	    fi

Then also add it in the following "else" statement:

        else
	        mkdir -p $rompath/.repo/local_manifests
	        echo -e ${CL_CYN}""${CL_RST}

	        if [ -d $rompath/vendor/bliss/ ]; then
	          # Take action if $DIR exists. #
	          export ROM_IS_BLISS=true
	          echo -e ${CL_CYN}"copying bliss specific manifest files..."${CL_RST}
	          cp -r $rompath/vendor/$vendor_path/manifests/android_pc/bliss/* $rompath/.repo/local_manifests
	        fi
	        if [ -d $rompath/vendor/lineage/ ]; then
	          # Take action if $DIR exists. #
	          export ROM_IS_LINEAGE=true
	          echo -e ${CL_CYN}"copying lineage specific manifest files..."${CL_RST}
	          cp -r $rompath/vendor/$vendor_path/manifests/android_pc/lineage/* $rompath/.repo/local_manifests
	        fi
	        if [ -d $rompath/vendor/tesla/ ]; then
	          # Take action if $DIR exists. #
	          export ROM_IS_TESLA=true
	          echo -e ${CL_CYN}"copying tesla specific manifest files..."${CL_RST}
	          cp -r $rompath/vendor/$vendor_path/manifests/android_pc/tesla/* $rompath/.repo/local_manifests
	        fi
	        if [ -d $rompath/vendor/tipsy/ ]; then
	          # Take action if $DIR exists. #
	          export ROM_IS_TIPSY=true
	          echo -e ${CL_CYN}"copying tipsy specific manifest files..."${CL_RST}
	          cp -r $rompath/vendor/$vendor_path/manifests/android_pc/tipsy/* $rompath/.repo/local_manifests
	        fi
	        echo -e ${CL_CYN}""${CL_RST}

Next script we have to edit is located in the bootable/newinstaller/Android.mk file

    $ nano bootable/newinstaller/Android.mk

Scroll down to the "# Use vendor defined version names" section (about line 129) and add your new ROM_IS_VENDORNAMEHERE to coincide with your vendor's version naming variable. 

    # Use vendor defined version names
    ROM_VENDOR_VERSION := $(TARGET_PRODUCT)-$(shell date +%Y%m%d%H%M)

    ifeq ($(ROM_IS_BLISS),true)
    ROM_VENDOR_VERSION := $(BLISS_VERSION)-$(TARGET_PRODUCT)
    endif

    ifeq ($(ROM_IS_LINEAGE),true)
    ROM_VENDOR_VERSION := $(LINEAGE_VERSION)-$(TARGET_PRODUCT)
    endif

    ifeq ($(ROM_IS_TESLA),true)
    ROM_VENDOR_VERSION := $(TESLA_VERSION)-$(TARGET_PRODUCT)
    endif

    ifeq ($(ROM_IS_TIPSY),true)
    ROM_VENDOR_VERSION := $(TIPSY_VERSION)-$(TARGET_PRODUCT)
    endif

After that, we can get on with the Manifest changes. 

### Updating the PC Manifests

The manifests are how we replace / add all the needed things to build Android-x86 based iso images. 
So starting from vendor/android-generic/manifests/android_pc, make a copy of one of the vendor folders that is closest to your current vendor setup
ex: (Bliss, Lineage, Tipsy, Tesla) and rename the folder to your vendor name. 

Then in a terminal type:

	$ . build/envsetup.sh
	$ build-x86 -s

Things should start to sync, and you should expect failures at this point. 
If a duplicate is found, edit the 01remove.xml to include or uncomment that remove:
	
	$ nano vendor/android-generic/manifests/android_pc/__YourVendorName__/01remove.xml
	

(Warning: After builds start to get going, you might run innto errors where something from the manifest is still needed. 
Please refer back to this step for that solution)

If a project is found missing, edit the 05other.xml to add that project. 

	$ nano vendor/android-generic/manifests/android_pc/__YourVendorName__/05other.xml


### Resolving Patch Conflicts

For the patches, after the first Manifest steps are done, you will want to then make a copy of all the results of the patch scripts. 
Each of the patches applied either resulted in "Applying", "Already applied", or "Conflicts". The only ones we want to pay attention to here are the "Conflicts", 
but only half of them. 

Some of the patches have duplicates for different vendor setups. So you will sometimes get results that look like this:

	Conflicts          system/core/0015-Bliss-init-don-t-bail-out-even-no-SELinux-domain-defined.patch
    Applying          system/core/0015-Tesla-init-don-t-bail-out-even-no-SELinux-domain-defined.patch 0
    
Notice how the one starting with "0015-Bliss-" failed, but the patch starting with "0015-Tesla-" applied correctly? If that happend on your vendor setup, you can ignore that patch. 
But if you only see one patch that had a "Conflicts", that will need to be applied and fixed. 

First, you apply the patch manually:

	$ git am "__patchLocationHere__"
	
You can expect that to fail, but it's crutial to the next step. Next you patch the file again, but using the "patch" command

	$ patch -p1 < "__patchLocationHere__"
	
This will generate the .orig & .rej files to help narrow down what you need to fix. After applying those patches to your local project folder, remember to generate the patch 
needed to resolve that conflict:

	$ git format-patch -1
	
Then copy the patch to the appropriate folder for the conflict. Example:

	$ cp system/core/0001-init-don-t-bail-out-even-no-SELinux-domain-defined.patch vendor/android-generic/patches/google_diff/x86/system/core/0015-__YourVendorName__-init-don-t-bail-out-even-no-SELinux-domain-defined.patch


# Updating the Script Repo
-------------------

Next step is to add your vendor changes to the script repo for everyone else to be able to benefit from it.
Submit your changes as a pull request to https://gitlab.com/android-generic/vendor_android-generic

# Credits

We'd like to say thanks to all these great individuals first:
@phhusson @cwhuang @maurossi @goffioul @me176c-dev @bosconovic @farmerbb @aclegg2011 and many others

And these great teams second:
@Google @LineageOS @GZR @OmniROM @SlimROM @ParanoidAndroid and many others, for you still lead the way for Open Innovation in the Android community. 

