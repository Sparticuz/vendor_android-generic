#!/bin/bash

rom_fp="$(date +%y%m%d)"
rompath=$(pwd)
vendor_path="android-generic"
mkdir -p release/$rom_fp/
set -e

#setup colors
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
purple=`tput setaf 5`
teal=`tput setaf 6`
light=`tput setaf 7`
dark=`tput setaf 8`
CL_CYN=`tput setaf 12`
CL_RST=`tput sgr0`
reset=`tput sgr0`

localManifestBranch="q-x86"
rom="Android-PC"
bliss_variant=""
bliss_variant_name=""
bliss_release="n"
bliss_partiton=""
filename=""
file_size=""
clean="n"
sync="n"
patch="n"
proprietary="n"
oldproprietary="n"
romBranch=""
desktopmode="n"
ipts_drivers="n"
efi_img="n"
rpm="n"
subsync="n"
kernel="n"
kername=""

if [ -z "$USER" ];then
        export USER="$(id -un)"
fi
export LC_ALL=C

if [[ $(uname -s) = "Darwin" ]];then
        jobs=$(sysctl -n hw.ncpu)
elif [[ $(uname -s) = "Linux" ]];then
        jobs=$(nproc)
fi


while test $# -gt 0
do
  case $1 in

  # Normal option processing
    -h | --help)
      echo "Usage: $0 options buildVariants blissBranch/extras"
      echo "options: -s | --sync: Repo syncs the rom (clears out patches), then reapplies patches to needed repos"
      echo "		 -p | --patch: Run the patches only"
      echo "		 -r | --proprietary: build needed items from proprietary vendor (non-public)"
      echo "		 -d | --desktopmode: Duild without any traditional launchers and only use Taskbar/TSL from @farmerbb"
      echo "buildVariants: "
      echo "android_x86-user, android_x86-userdebug, android_x86-eng,  "
      echo "android_x86_64-user, android_x86_64-userdebug, android_x86_64-eng"
      echo "blissBranch: select which bliss branch to sync, default is p9.0"
      echo "extras: specify 'foss', 'go', 'gms', 'gapps' or 'none' to be built in"
      echo ""
      ;;
    -c | --clean)
      clean="y";
      echo "Cleaning build and device tree selected."
      ;;
    -v | --version)
      echo "Version: Bliss x86 Builder 2.0"
      echo "Updated: 10/19/2019"
      ;;
    -s | --sync)
      sync="y";
      echo "Repo syncing and patching selected."
      ;;
    -p | --patch)
      patch="y";
      echo "patching selected."
      ;;
    -r | --proprietary)
      proprietary="y";
      echo "proprietary selected."
      ;;
    -o | --oldproprietary)
      oldproprietary="y";
	  echo -e ${CL_CYN}""${CL_CYN}
	  echo -e ${CL_CYN}"====-Bliss-OS(x86) Setting Up Private Houdini-===="${CL_RST}
	  echo -e ${CL_CYN}"				Let the Games Begin 				"${CL_RST}
	  echo -e ${CL_CYN}"======================================================"${CL_RST}
	  echo -e ""
      echo "old proprietary selected."
      ;;
    -d | --desktopmode)
	  desktopmode="y";
	  echo "desktop mode selected"
      ;;
    -i | --iptsdrivers)
	  ipts_drivers="y";
	  echo "Adding IPTS Drivers"
      ;;
    -e | --efi_img)
	  efi_img="y";
	  echo "building EFI image"
      ;;
    -m | --rpm)
	  rpm="y";
	  echo "building RPM installer"
      ;;
    -u | --subsync)
	  subsync="y";
	  echo "syncing native bridge submodules"
	  ;;
	-k | --kernel)
	  kernel="y";
	  echo "non-default kernel selected. You will be prompted to specify what kernel."
	  ;;
  # ...

  # Special cases
    --)
      break
      ;;
    --*)
      # error unknown (long) option $1
      ;;
    -?)
      # error unknown (short) option $1
      ;;

  # FUN STUFF HERE:
  # Split apart combined short options
    -*)
      split=$1
      shift
      set -- $(echo "$split" | cut -c 2- | sed 's/./-& /g') "$@"
      continue
      ;;

  # Done with options
    *)
      break
      ;;
  esac

  # for testing purposes:
  shift
done


if [ "$1" = "android_x86_64-user" ];then
        bliss_variant=android_x86_64-user;
        bliss_variant_name=android_x86_64-user;

elif [ "$1" = "android_x86_64-userdebug" ];then
        bliss_variant=android_x86_64-userdebug;
        bliss_variant_name=android_x86_64-userdebug;

elif [ "$1" = "android_x86_64-eng" ];then
        bliss_variant=android_x86_64-eng;
        bliss_variant_name=android_x86_64-eng;

elif [ "$1" = "android_x86-eng" ];then
        bliss_variant=android_x86-eng;
        bliss_variant_name=android_x86-eng;

elif [ "$1" = "android_x86-userdebug" ];then
        bliss_variant=android_x86-userdebug;
        bliss_variant_name=android_x86-userdebug;

elif [ "$1" = "android_x86-eng" ];then
        bliss_variant=android_x86-eng;
        bliss_variant_name=android_x86-eng;
else
	echo "you need to at least use '--help'"

fi

echo -e ${CL_CYN}""${CL_CYN}
echo -e ${CL_CYN}" Android-Generic Project addons"${CL_RST}
echo -e ${CL_CYN}""${CL_CYN}

if [ "$2" = "foss" ];then
   export USE_OPENGAPPS=false
   export USE_FDROID=false
   export USE_FOSS=true
   export USE_GO=false
   export USE_GMS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"======-Bliss-OS(x86) Building w/ microG & F-Droid-====="${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "gapps" ];then
   export USE_FOSS=false
   export USE_FDROID=false
   export USE_GO=false
   export USE_OPENGAPPS=true
   export USE_GMS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"=========-Bliss-OS(x86) Building w/ OpenGapps-========="${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "go" ];then
   export USE_GO=true
   export USE_FDROID=false
   export USE_FOSS=false
   export USE_OPENGAPPS=false
   export USE_GMS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"=============-Bliss-OS(x86) Building w/ Go-============"${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "none" ];then
   export USE_FOSS=false
   export USE_FDROID=false
   export USE_GO=false
   export USE_OPENGAPPS=false
   export USE_GMS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"=============-Bliss-OS(x86) Building Clean-============"${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "gms" ];then
   export USE_GMS=true
   export USE_FDROID=false
   export USE_FOSS=false
   export USE_GO=false
   export USE_OPENGAPPS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"===========-Bliss-OS(x86) Building with GMS-==========="${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

elif [ "$2" = "fdroid" ];then
   export USE_FDROID=true
   export USE_GMS=false
   export USE_FOSS=false
   export USE_GO=false
   export USE_OPENGAPPS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"=========-Bliss-OS(x86) Building with FDROID-=========="${CL_RST}
   echo -e ${CL_CYN}"       Please consider contributing to our project"     ${CL_RST}
   echo -e ${CL_CYN}"====================================================="${CL_RST}
   echo -e ""

else
   export USE_FDROID=false
   export USE_GMS=false
   export USE_FOSS=false
   export USE_GO=false
   export USE_OPENGAPPS=false
   echo -e ${CL_CYN}""${CL_CYN}
   echo -e ${CL_CYN}"======-Bliss-OS(x86) Building with no additions-======"${CL_RST}
   echo -e ${CL_CYN}"      Please consider contributing to our project"       ${CL_RST}
   echo -e ${CL_CYN}"======================================================"${CL_RST}
   echo -e ""

fi

if [ "$3" = "croshoudini" ];then
	export USE_HOUDINI=true
	export USE_PRIV_HOUDINI=false
	export USE_WIDEVINE=false
	export DONT_SET_NB_ABI=false
	export USE_X86LIBNB=false
	echo -e ${CL_CYN}""${CL_CYN}
	echo -e ${CL_CYN}"======-Bliss-OS(x86) Building with Houdini addon-====="${CL_RST}
	echo -e ${CL_CYN}"Source: https://github.com/me176c-dev/android_vendor_google_chromeos-x86 "${CL_RST}
	echo -e ${CL_CYN}"======================================================"${CL_RST}
	echo -e ""

elif [ "$3" = "crosboth" ];then
	export USE_HOUDINI=true
	export USE_WIDEVINE=true
	export USE_PRIV_HOUDINI=false
	export DONT_SET_NB_ABI=false
	export USE_X86LIBNB=false
	echo -e ${CL_CYN}""${CL_CYN}
	echo -e ${CL_CYN}"====-Bliss-OS(x86) Building w/ Widevine & Houdini-===="${CL_RST}
	echo -e ${CL_CYN}"Source: https://github.com/me176c-dev/android_vendor_google_chromeos-x86 "${CL_RST}
	echo -e ${CL_CYN}"======================================================"${CL_RST}
	echo -e ""

elif [ "$3" = "crospriv" ];then
	export USE_HOUDINI=false
	export USE_PRIV_HOUDINI=true
	export USE_WIDEVINE=true
	export DONT_SET_NB_ABI=false
	export USE_X86LIBNB=false
	echo -e ${CL_CYN}""${CL_CYN}
	echo -e ${CL_CYN}"====-Bliss-OS(x86) Building w/ Private Houdini-===="${CL_RST}
	echo -e ${CL_CYN}"Source:  What you talkin bout Yonis? "${CL_RST}
	echo -e ${CL_CYN}"======================================================"${CL_RST}
	echo -e ""

elif [ "$3" = "crosnone" ];then
	export USE_HOUDINI=false
	export USE_WIDEVINE=false
	export USE_PRIV_HOUDINI=false
	export DONT_SET_NB_ABI=true
	export USE_X86LIBNB=false
	echo -e ${CL_CYN}""${CL_CYN}
	echo -e ${CL_CYN}"======-Bliss-OS(x86) Building with just no addons-======"${CL_RST}
	echo -e ${CL_CYN}"      Please consider contributing to our project"       ${CL_RST}
	echo -e ${CL_CYN}"======================================================"${CL_RST}
	echo -e ""

elif [ "$3" = "x86nb" ];then
	export USE_HOUDINI=true
	export USE_WIDEVINE=true
	export USE_PRIV_HOUDINI=false
	export DONT_SET_NB_ABI=true
	export USE_X86LIBNB=true
	echo -e ${CL_CYN}""${CL_CYN}
	echo -e ${CL_CYN}"======-Bliss-OS(x86) Building w/ ax86-nb & widevine-======"${CL_RST}
	echo -e ${CL_CYN}"	x86-NB Source: https://github.com/goffioul/ax86-nb-qemu   "${CL_RST}
	echo -e ${CL_CYN}"	and Widevine from Chrome OS  		     "${CL_RST}
	echo -e ${CL_CYN}"======================================================"${CL_RST}
	echo -e ""
	subsync="y";

else
	export USE_WIDEVINE=true
	export USE_PRIV_HOUDINI=false
	export USE_HOUDINI=false
	export DONT_SET_NB_ABI=true
	export USE_X86LIBNB=false
	echo -e ${CL_CYN}""${CL_CYN}
	echo -e ${CL_CYN}"======-Bliss-OS(x86) Building with just widevine-======"${CL_RST}
	echo -e ${CL_CYN}"      Please consider contributing to our project"       ${CL_RST}
	echo -e ${CL_CYN}"======================================================"${CL_RST}
	echo -e ""

fi

if [ $desktopmode == "y" ];then
	export BLISS_DESKTOPMODE=true
else
	export BLISS_DESKTOPMODE=false
fi

if [ $ipts_drivers == "y" ];then
	export IPTS_DRIVERS=true
else
	export IPTS_DRIVERS=false
fi

if  [ $sync == "y" ];then
	rm -rf $rompath/.repo/local_manifests/*
if [ -d $rompath/.repo/local_manifests ] ;then
	echo -e ${CL_CYN}""${CL_RST}

	# Add AOSP manifests, if another vendor is found, overwrite
	cp -r $rompath/vendor/$vendor_path/manifests/android_pc/aosp/* $rompath/.repo/local_manifests

	if [ -d $rompath/vendor/bliss/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_BLISS=true
	  echo -e ${CL_CYN}"copying bliss specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/android_pc/bliss/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/lineage/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_LINEAGE=true
	  echo -e ${CL_CYN}"copying lineage specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/android_pc/lineage/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/tesla/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_TESLA=true
	  echo -e ${CL_CYN}"copying tesla specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/android_pc/tesla/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/tipsy/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_TIPSY=true
	  echo -e ${CL_CYN}"copying tipsy specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/android_pc/tipsy/* $rompath/.repo/local_manifests
	fi
	echo -e ${CL_CYN}""${CL_RST}
	# Device type selection
	PS3='Which device type do you plan on building?: '
	options=("Vulkan x86/x86_64"
			 "Atom x86/x86_64"
			 "Generic x86/x86_64")
	select opt in "${options[@]}"
	do
		case $opt in
			"Vulkan x86/x86_64")
				echo "you chose choice $REPLY which is $opt"
				cp -r $rompath/vendor/$vendor_path/manifests/android_pc/device/vulkan/* $rompath/.repo/local_manifests
				break
				;;
			"Atom x86/x86_64")
				echo "you chose choice $REPLY which is $opt"
				cp -r $rompath/vendor/$vendor_path/manifests/android_pc/device/atom/* $rompath/.repo/local_manifests
				break
				;;
			"Generic x86/x86_64")
				echo "you chose choice $REPLY which is $opt"
				cp -r $rompath/vendor/$vendor_path/manifests/android_pc/device/generic/* $rompath/.repo/local_manifests
				break
				;;
			*) echo "invalid option $REPLY";;
		esac
	done

	# Copy rest of generic manifests.
	cp -r $rompath/vendor/$vendor_path/manifests/android_pc/local_manifests/* $rompath/.repo/local_manifests
else
	mkdir -p $rompath/.repo/local_manifests
	echo -e ${CL_CYN}""${CL_RST}

	# Add AOSP manifests, if another vendor is found, overwrite
	cp -r $rompath/vendor/$vendor_path/manifests/android_pc/aosp/* $rompath/.repo/local_manifests

	if [ -d $rompath/vendor/bliss/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_BLISS=true
	  echo -e ${CL_CYN}"copying bliss specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/android_pc/bliss/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/lineage/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_LINEAGE=true
	  echo -e ${CL_CYN}"copying lineage specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/android_pc/lineage/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/tesla/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_TESLA=true
	  echo -e ${CL_CYN}"copying tesla specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/android_pc/tesla/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/tipsy/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_TIPSY=true
	  echo -e ${CL_CYN}"copying tipsy specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/android_pc/tipsy/* $rompath/.repo/local_manifests
	fi
	echo -e ${CL_CYN}""${CL_RST}

	# Device type selection
	PS3='Which device type do you plan on building?: '
	options=("Vulkan x86/x86_64"
			 "Atom x86/x86_64"
			 "Generic x86/x86_64")
	select opt in "${options[@]}"
	do
		case $opt in
			"Vulkan x86/x86_64")
				echo "you chose choice $REPLY which is $opt"
				cp -r $rompath/vendor/$vendor_path/manifests/android_pc/device/vulkan/* $rompath/.repo/local_manifests
				break
				;;
			"Atom x86/x86_64")
				echo "you chose choice $REPLY which is $opt"
				cp -r $rompath/vendor/$vendor_path/manifests/android_pc/device/atom/* $rompath/.repo/local_manifests
				break
				;;
			"Generic x86/x86_64")
				echo "you chose choice $REPLY which is $opt"
				cp -r $rompath/vendor/$vendor_path/manifests/android_pc/device/generic/* $rompath/.repo/local_manifests
				break
				;;
			*) echo "invalid option $REPLY";;
		esac
	done

	cp -r $rompath/vendor/$vendor_path/manifests/android_pc/local_manifests/* $rompath/.repo/local_manifests
fi
echo -e ${CL_CYN}""${CL_RST}
echo -e ${CL_CYN}""${CL_RST}
echo -e ${CL_CYN}"Syncing Files..."${CL_RST}
echo -e ${CL_CYN}""${CL_RST}
echo -e ${CL_CYN}""${CL_RST}

repo sync -c -j$jobs --no-tags --no-clone-bundle --force-sync

else
        echo "Not gonna sync this round"
fi

if  [ $subsync == "y" ];then
	cd external/ax86-nb-qemu && git submodule update --init --recursive
	# Then we get some more stuff


	cd ..
	cd ..
fi

if [ $clean == "y" ];then
	echo "Cleaning up a bit"
    make clean && make clobber
fi

if  [ $sync == "y" ];then
	echo "Let the patching begin"
	bash "$rompath/vendor/$vendor_path/prepatch_vendor_pc.sh"
	bash "$rompath/vendor/$vendor_path/autopatch_x86.sh"
	bash "$rompath/vendor/$vendor_path/autopatch_vendor_pc.sh"
fi

if  [ $patch == "y" ];then
	echo "Let the patching begin"
	bash "$rompath/vendor/$vendor_path/prepatch_vendor_pc.sh"
	bash "$rompath/vendor/$vendor_path/autopatch_x86.sh"
	bash "$rompath/vendor/$vendor_path/autopatch_vendor_pc.sh"
fi


if [[ "$1" = "android_x86_64-user" || "$1" = "android_x86_64-userdebug" || "$1" = "android_x86_64-eng" || "$1" = "android_x86-user" || "$1" = "android_x86-userdebug" || "$1" = "android_x86-eng" ]];then
echo "Setting up build env for: $1"
	. build/envsetup.sh
fi

if  [ $kernel == "y" ];then
	echo ""
	echo "${red}Kernel Choice ${reset}"
	echo ""
	echo "${green}Which kernel branch do you want to build with? ${reset}"
	echo "( BR-x86/k4.19.110-ax86-ga-rmi, BR-x86/k4.19.110-ax86-ga, BR-x86/k4.19.110-ax86-ga-rmi-bytr, BR-x86/k5.6.2-si-yling, BR-x86/k5.6.2-si-yling-rmi, BR-x86/android-mainline-5.7rc2-lx86)"
	echo "${green}Or specify a different valid branch (cd kernel && git branch -a for a list of valid branches) ${reset}"
	echo ""
	read kername
	echo ""
	echo "${green}ok, switching to branch: $kername ${reset}"
	echo ""

	cd kernel && git checkout $kername && make clean && make mrproper

	cd ..

fi

buildProprietary() {
	# Start with grabbing our Chrome OS bits.
	echo "Setting up Proprietary environment for: $1"
	lunch $bliss_variant
	echo "Building proprietary tools, part 1... This won't take too long..."
	cd vendor/google/chromeos-x86
	./extract-files.sh
	cd ..
	cd ..
	cd ..

	# Now we also download our fdroid apps.
	cd vendor/foss
	bash update.sh
	cd ..
	cd ..

}

buildOldProprietary() {
	echo "Setting up Proprietary environment for: $1"
	lunch $bliss_variant
	echo "Building proprietary tools, part 1... This won't take too long..."
	mka update_engine_applier
	echo "Building proprietary tools... part 2... This may take a while..."
	mka proprietary
}

buildVariant() {
	echo "Starting lunch command for: $1"
	lunch $1
	echo "Starting up the build... This may take a while..."
	if [ $efi_img == "y" ];then
		echo -e ${CL_CYN}""${CL_CYN}
		echo -e ${CL_CYN}"======-Bliss-OS(x86) Building as EFI image-======"${CL_RST}
		echo -e ${CL_CYN}"      This image uses rEFInd to boot instead of Grub2 "       ${CL_RST}
		echo -e ${CL_CYN}"======================================================"${CL_RST}
		echo -e ""
		make -j$((`nproc`-2)) efi_img
	elif [ $rpm == "y" ];then
		echo -e ${CL_CYN}""${CL_CYN}
		echo -e ${CL_CYN}"======-Bliss-OS(x86) Building as RPM installer-======"${CL_RST}
		echo -e ${CL_CYN}"      This image uses rEFInd to boot instead of Grub2 "       ${CL_RST}
		echo -e ${CL_CYN}"======================================================"${CL_RST}
		echo -e ""
		make -j$((`nproc`-2)) rpm
	else
		echo -e ${CL_CYN}""${CL_CYN}
		echo -e ${CL_CYN}"======-Bliss-OS(x86) Building as ISO -======"${CL_RST}
		echo -e ${CL_CYN}"This image will use Grub2 for booting on MBR/EFI "       ${CL_RST}
		echo -e ${CL_CYN}"======================================================"${CL_RST}
		echo -e ""
		make -j$((`nproc`-2)) iso_img
		# nproc | xargs -I % make -j% iso_img
	fi
}

if  [ $proprietary == "y" ];then
	. build/envsetup.sh
	buildProprietary $bliss_variant
fi

if  [ $oldproprietary == "y" ];then
	. build/envsetup.sh
	buildOldProprietary $bliss_variant
fi

if [[ "$1" = "android_x86_64-user" || "$1" = "android_x86_64-userdebug" || "$1" = "android_x86_64-eng" || "$1" = "android_x86-user" || "$1" = "android_x86-userdebug" || "$1" = "android_x86-eng" ]];then
	buildVariant $bliss_variant $bliss_variant_name
fi
