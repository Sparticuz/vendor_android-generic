#!/bin/bash

rom_fp="$(date +%y%m%d)"
rompath=$(pwd)
vendor_path="android-generic"
mkdir -p release/$rom_fp/
# Comment out 'set -e' if your session terminates.
set -e

#setup colors
red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
blue=`tput setaf 4`
purple=`tput setaf 5`
teal=`tput setaf 6`
light=`tput setaf 7`
dark=`tput setaf 8`
CL_CYN=`tput setaf 12`
CL_RST=`tput sgr0`
reset=`tput sgr0`

localManifestBranch="q"
rom="bliss"
ag_variant=""
ag_variant_name=""
ag_release="n"
bliss_partiton=""
filename=""
file_size=""
clean="n"
sync="n"
patch="n"
romBranch=""
ver=$(date +"%F")

if [ -z "$USER" ];then
        export USER="$(id -un)"
fi
export LC_ALL=C

if [[ $(uname -s) = "Darwin" ]];then
        jobs=$(sysctl -n hw.ncpu)
elif [[ $(uname -s) = "Linux" ]];then
        jobs=$(nproc)
fi


while test $# -gt 0
do
  case $1 in

  # Normal option processing
    -h | --help)
      echo "Usage: $0 options buildVariants blissBranch"
      echo "options: -c | --clean : Does make clean && make clobber and resets the treble device tree"
      echo "         -r | --release : Builds a twrp zip of the rom (only for A partition) default creates system.img"
      echo "         -s | --sync: Repo syncs the rom (clears out patches), then reapplies patches to needed repos"
      echo ""
      echo "buildVariants: arm64_a_stock | arm64_ab_stock : Vanilla Rom"
      echo "                arm64_a_gapps | arm64_ab_gapps : Stock Rom with Gapps Built-in"
      echo "                arm64_a_foss | arm64_ab_foss : Stock Rom with Foss"
      echo "                arm64_a_go | arm64_ab_go : Stock Rom with Go-Gapps"
      echo ""
      echo "blissBranch: select which bliss branch to sync, default is o8.1-los"
      ;;
    -v | --version)
      echo "Version: Bliss Treble Builder 0.3"
      echo "Updated: 7/29/2018"
      ;;
    -c | --clean)
      clean="y";
      echo "Cleaning build and device tree selected."
      ;;
    -r | --release)
      ag_release="y";
      echo "Building as release selected."
      ;;
    -s | --sync)
      sync="y"
      echo "Repo syncing and patching selected."
      ;;
    -p | --patch)
      patch="y";
      echo "patching selected."
      ;;
  # ...

  # Special cases
    --)
      break
      ;;
    --*)
      # error unknown (long) option $1
      ;;
    -?)
      # error unknown (short) option $1
      ;;

  # FUN STUFF HERE:
  # Split apart combined short options
    -*)
      split=$1
      shift
      set -- $(echo "$split" | cut -c 2- | sed 's/./-& /g') "$@"
      continue
      ;;

  # Done with options
    *)
      break
      ;;
  esac

  # for testing purposes:
  shift
done

if [ "$1" = "arm64_a_stock" ];then
        ag_variant=treble_arm64_avN-userdebug;
        ag_variant_name=arm64-a-stock;
        ag_partition="a";

elif [ "$1" = "arm64_a_gapps" ];then
        ag_variant=treble_arm64_agS-userdebug;
        ag_variant_name=arm64-a-gapps;
        ag_partition="a";

elif [ "$1" = "arm64_a_foss" ];then
        ag_variant=treble_arm64_afS-userdebug;
        ag_variant_name=arm64-a-foss;
        ag_partition="a";

elif [ "$1" = "arm64_a_go" ];then
        ag_variant=treble_arm64_aoS-userdebug;
        ag_variant_name=arm64-a-go;
        ag_partition="a";

elif [ "$1" = "arm64_ab_stock" ];then
        ag_variant=treble_arm64_bvN-userdebug;
        ag_variant_name=arm64-ab-stock;
        ag_partition="ab";

elif [ "$1" = "arm64_ab_gapps" ];then
        ag_variant=treble_arm64_bgS-userdebug;
        ag_variant_name=arm64-ab-gapps;
        ag_partition="ab";

elif [ "$1" = "arm64_ab_foss" ];then
        ag_variant=treble_arm64_bfS-userdebug;
        ag_variant_name=arm64-ab-foss;
        ag_partition="ab";

elif [ "$1" = "arm64_ab_go" ];then
        ag_variant=treble_arm64_boS-userdebug;
        ag_variant_name=arm64-ab-go;
        ag_partition="ab";
else
        echo "you need to at least use '--help'"
fi

if [ "$2" = "" ];then
   romBranch="q"
   echo "Using branch $romBranch for repo syncing Bliss."
else
   romBranch="$2"
   echo "Using branch $romBranch for repo syning Bliss."
fi


if [ $sync == "y" ];then
	rm -rf $rompath/.repo/local_manifests/*
if [ -d $rompath/.repo/local_manifests ] ;then
	echo -e ${CL_CYN}""${CL_RST}

	if [ -d $rompath/vendor/bliss/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_BLISS=true
	  echo -e ${CL_CYN}"copying bliss specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/treble/bliss/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/lineage/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_LINEAGE=true
	  echo -e ${CL_CYN}"copying lineage specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/treble/lineage/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/tesla/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_TESLA=true
	  echo -e ${CL_CYN}"copying tesla specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/treble/tesla/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/tipsy/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_TIPSY=true
	  echo -e ${CL_CYN}"copying tipsy specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/treble/tipsy/* $rompath/.repo/local_manifests
	fi
	echo -e ${CL_CYN}""${CL_RST}
	
	# Copy rest of generic manifests. 
	cp -r $rompath/vendor/$vendor_path/manifests/treble/local_manifests/* $rompath/.repo/local_manifests
else
	mkdir -p $rompath/.repo/local_manifests
	echo -e ${CL_CYN}""${CL_RST}

	if [ -d $rompath/vendor/bliss/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_BLISS=true
	  echo -e ${CL_CYN}"copying bliss specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/treble/bliss/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/lineage/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_LINEAGE=true
	  echo -e ${CL_CYN}"copying lineage specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/treble/lineage/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/tesla/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_TESLA=true
	  echo -e ${CL_CYN}"copying tesla specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/treble/tesla/* $rompath/.repo/local_manifests
	fi
	if [ -d $rompath/vendor/tipsy/ ]; then
	  # Take action if $DIR exists. #
	  export ROM_IS_TIPSY=true
	  echo -e ${CL_CYN}"copying tipsy specific manifest files..."${CL_RST}
	  cp -r $rompath/vendor/$vendor_path/manifests/treble/tipsy/* $rompath/.repo/local_manifests
	fi
	echo -e ${CL_CYN}""${CL_RST}
	
	cp -r $rompath/vendor/$vendor_path/manifests/treble/local_manifests/* $rompath/.repo/local_manifests
fi

repo sync -c -j$jobs --no-tags --no-clone-bundle --force-sync

else
        echo "Not gonna sync this round"
fi



rm -f device/*/sepolicy/common/private/genfs_contexts

if [ $clean == "y" ];then
    (cd device/phh/treble; git clean -fdx; bash generate.sh $rom)
    make clean && make clobber
else
    (cd device/phh/treble; bash generate.sh $rom)
fi

sed -i -e 's/BOARD_SYSTEMIMAGE_PARTITION_SIZE := 1610612736/BOARD_SYSTEMIMAGE_PARTITION_SIZE := 2147483648/g' device/phh/treble/phhgsi_arm64_a/BoardConfig.mk

#if  [ $sync == "y" ];then
#if [ -z "$local_patches" ];then
#    if [ -d patches ];then
#        ( cd vendor/bliss/treble/patches/patches; git fetch; git reset --hard; git checkout origin/$localManifestBranch)
#    else
#        git clone https://github.com/aclegg2011/treble_patches vendor/bliss/treble/patches -b $localManifestBranch
#    fi
#else
#    rm -Rf vendor/bliss/treble/patches/patches
#    mkdir vendor/bliss/treble/patches/patches
#    unzip  "$local_patches" -d patches
#fi
#echo "Let the patching begin"
#bash "$rompath/vendor/bliss/treble/apply-patches.sh" $rompath/vendor/bliss/treble/patches/patches
#fi

if [ $sync == "y" ];then
echo "Let the patching begin"
bash "$rompath/vendor/$vendor_path/prepatch_vendor_treble.sh"
bash "$rompath/vendor/$vendor_path/autopatch_treble.sh"
bash "$rompath/vendor/$vendor_path/autopatch_vendor_treble.sh"
fi

#blissRelease(){
#if [[ "$1" = "y" && $ag_partition = "a" ]];then
#       echo "Building twrp flashable gsi.."
#        if [ -d $OUT/img2sdat ] ;then
#             cp -r $rompath/build/make/core/treble/img2sdat/* $OUT/img2sdat
#         else
#             mkdir -p $OUT/img2sdat
#             cp -r $rompath/build/make/core/treble/img2sdat/* $OUT/img2sdat
#        fi
#        if [ -d $OUT/twrp_flashables ] ;then
#             cp -r $rompath/build/make/core/treble/twrp_flashables/* $OUT/twrp_flashables
#         else
#             mkdir -p $OUT/twrp_flashables
#             cp -r $rompath/build/make/core/treble/twrp_flashables/* $OUT/twrp_flashables
#        fi
#        cp $OUT/system.img $OUT/img2sdat/system.img
#        cd $OUT/img2sdat
#        ./img2sdat.py system.img -o tmp -v 4
#        cd $rompath
#        cp $OUT/img2sdat/tmp/* $OUT/twrp_flashables/arm64a
#        cd $OUT/twrp_flashables/arm64a
#        7za a -tzip arm64a.zip *
#        cd $rompath
#        cp $OUT/twrp_flashables/arm64a/arm64a.zip $rompath/release/$rom_fp/Bliss-$ver-$ag_variant_name.zip
#        rm -rf $OUT/img2sdat $OUT/twrp_flashables
#        filename=Bliss-$ver-$ag_variant_name.zip
#        filesize=$(stat -c%s release/$rom_fp/$filename)
#        md5sum release/$rom_fp/Bliss-$ver-$ag_variant_name.zip > release/$rom_fp/Bliss-$ver-$ag_variant_name.zip.md5
#        md5sum_file=Bliss-$ver-$ag_variant_name.zip.md5
#else
#        if  [[ "$1" = "n" || $ag_partition = "ab" ]];then
#        echo "Twrp Building is currently not suported on a/b"
#        fi
#        echo "Compressing and Copying $OUT/system.img -> release/$rom_fp/Bliss-$ver-$ag_variant_name.img.xz"
#        xz -c $OUT/system.img > release/$rom_fp/Bliss-$ver-$ag_variant_name.img.xz
#        filename=Bliss-$ver-$ag_variant_name.img.xz
#        filesize=$(stat -c%s release/$rom_fp/$filename)
#        md5sum release/$rom_fp/Bliss-$ver-$ag_variant_name.img.xz  > release/$rom_fp/Bliss-$ver-$ag_variant_name.img.xz.md5
#        md5sum_file=Bliss-$ver-$ag_variant_name.img.xz.md5
#fi
#}

if  [ $patch == "y" ];then
	echo "Let the patching begin"
	bash "$rompath/vendor/$vendor_path/prepatch_vendor_treble.sh"
	bash "$rompath/vendor/$vendor_path/autopatch_treble.sh"
	bash "$rompath/vendor/$vendor_path/autopatch_vendor_treble.sh"
fi

blissHeader(){
        file_size=$(echo "${filesize}" | awk '{ split( "B KB MB GB TB PB" , v ); s=1; while( $1>1024 ){ $1/=1024; s++ } printf "%.2f %s", $1, v[s] }')
        echo -e ""
        echo -e "      ___           ___                   ___           ___      "
        echo -e "     /\  \         /\__\      ___        /\  \         /\  \     "
        echo -e "    /::\  \       /:/  /     /\  \      /::\  \       /::\  \    "
        echo -e "   /:/\:\  \     /:/  /      \:\  \    /:/\ \  \     /:/\ \  \   "
        echo -e "  /::\~\:\__\   /:/  /       /::\__\  _\:\~\ \  \   _\:\~\ \  \  "
        echo -e " /:/\:\ \:\__\ /:/__/     __/:/\/__/ /\ \:\ \ \__\ /\ \:\ \ \__\ "
        echo -e " \:\~\:\/:/  / \:\  \    /\/:/  /    \:\ \:\ \/__/ \:\ \:\ \/__/ "
        echo -e "  \:\ \::/  /   \:\  \   \::/__/      \:\ \:\__\    \:\ \:\__\   "
        echo -e "   \:\/:/  /     \:\  \   \:\__\       \:\/:/  /     \:\/:/  /   "
        echo -e "    \::/__/       \:\__\   \/__/        \::/  /       \::/  /    "
        echo -e "     ~~            \/__/                 \/__/         \/__/     "
        echo -e ""
        echo -e "===========-Bliss Package Complete-==========="
        echo -e "File: $1"
        echo -e "MD5: $3"
        echo -e "Size: $file_size"
        echo -e "==============================================="
        echo -e "Have A Truly Blissful Experience"
        echo -e "==============================================="
        echo -e ""
}

if [[ "$1" = "arm64_a_stock" || "$1" = "arm64_a_gapps" || "$1" = "arm64_a_foss" || "$1" = "arm64_a_go" || "$1" = "arm64_ab_stock" || "$1" = "arm64_ab_gapps" || "$1" = "arm64_ab_foss" || "$1" = "arm64_ab_go" ]];then
echo "Setting up build env for $1"
. build/envsetup.sh
fi

buildVariant() {
                ## echo "running lunch for $1"
        ## lunch $1
        echo "Running lunch for $ag_variant"
        lunch $ag_variant
        make WITHOUT_CHECK_API=true BUILD_NUMBER=$rom_fp installclean
        make WITHOUT_CHECK_API=true BUILD_NUMBER=$rom_fp -j$jobs systemimage
        make WITHOUT_CHECK_API=true BUILD_NUMBER=$rom_fp vndk-test-sepolicy
        blissRelease $ag_release $ag_partition
        blissHeader $filename $filesize $md5sum_file
}

if [[ "$1" = "arm64_a_stock" || "$1" = "arm64_a_gapps" || "$1" = "arm64_a_foss" || "$1" = "arm64_a_go" || "$1" = "arm64_ab_stock" || "$1" = "arm64_ab_gapps" || "$1" = "arm64_ab_foss" || "$1" = "arm64_ab_go" ]];then
buildVariant $ag_variant $ag_variant_name
fi
